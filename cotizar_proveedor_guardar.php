<?php
include 'inc/conexion.php';
$refaccion_proveedor_id  = "";
$refaccion_id_post = $_POST['refaccion_id'];
$id_proveedor_post = strtoupper($_POST['idproveedor']);
$fecha_solicitud_post = strtoupper($_POST['fecha_solicitud']);
$precio_post = strtoupper($_POST['precio']);


$sel = $con->prepare("SELECT refaccion_proveedor_id ,id_refaccion,id_proveedor FROM refaccion_proveedor where id_refaccion=? AND id_proveedor=?");
$sel->bind_param('is', $refaccion_id_post, $id_proveedor_post);
$sel->execute();
$res = $sel->get_result();
$row = mysqli_num_rows($res);

if ($row != 0) {
    echo "YA EXISTE REGISTRO CON ESA REFACCION ";
    
} else {
    $ins = $con->prepare("INSERT INTO refaccion_proveedor VALUES(?,?,?,?,?)");
    $ins->bind_param("iiisd", $refaccion_proveedor_id, $refaccion_id_post, $id_proveedor_post, $fecha_solicitud_post, $precio_post);
    if ($ins->execute()) {
        header("Location:alerta.php?tipo=exito&operacion=CotizacionGuardada&destino=seleccionar_refaccion.php");
    } else {
        header("Location:alerta.php?tipo=fracaso&operacion=ErroralGuardarCotizacion&destino=seleccionar_refaccion.php");
    }
    $ins->close();
    $con->close();
}
?>
