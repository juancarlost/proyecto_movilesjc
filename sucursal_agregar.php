<!DOCTYPE html>
<html>
    <head>
        <title>TODO supply a title</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!--código que incluye Bootstrap-->
        <?php include'inc/incluye_bootstrap.php' ?>
        <!--termina código que incluye Bootstrap-->
        <?php include 'inc/conexion.php' ?>

    </head>
    <body>
        <!--código que incluye el menú responsivo-->
        <?php include'inc/incluye_menu.php' ?>
        <!--termina código que incluye el menú responsivo-->
        <div class="container">
            <div class="jumbotron">
                <form role="form" id="login-form" method="post" class="form-signin" action="sucursal_guardar.php">
                    <div class="h2">
                        Registrar una Sucursal
                    </div>
                    <div class="form-group">
                        <label>Proveedor </label><br>
                        <?php
                        //Consulta sin parámetros
                        $sel = $con->prepare("SELECT *from proveedor");
                        $sel->execute();
                        $res = $sel->get_result();
                        $row = mysqli_num_rows($res);
                        ?>
                        <select name="idproveedor">
                                <?php while ($f = $res->fetch_assoc()) { ?>
                                    <option value="<?php echo $f['proveedor_id'] ?>"><?php echo $f['proveedor_nombre'] ?></option>        
                                <?php
                                }
                                $sel->close();
                                $con->close();
                                ?>
                    </select>   
                     </div>
                    <div class="form-group">
                        <label>Nombre de la Sucursal (requerido) </label>
                        <input type="text" class="form-control" id="nombre_de_sucursal" name="nombre_de_sucursal"
                               placeholder="Ingresa nombre de la Sucursal" style="text-transform:uppercase;" required>
                    </div>
                    <div class="form-group">
                        <label>Dirección de la Sucursal (requerido)</label>
                        <input type="text" class="form-control" id="direccion_de_sucursal" name="direccion_de_sucursal"
                               placeholder="Ingresa la dirección (Tienda Matriz)" style="text-transform:uppercase;" required>
                    </div>
                    <div class="form-group">
                        <label>Tel&eacute;fono; 1</label>
                        <input type="text" class="form-control" id="telefono1_de_sucursal" name="telefono1_de_sucursal"
                               placeholder="Ingresa primer teléfono" style="text-transform:uppercase;" required>
                    </div>
                    <div class="form-group">
                        <label>Tel&eacute;fono 2</label>
                        <input type="text" class="form-control" id="telefono2_de_sucursal" name="telefono2_de_sucursal"
                               placeholder="Ingresa segundo teléfono" style="text-transform:uppercase;" required>
                    </div>
                    <div class="form-group">
                        <label>Correo electrónico</label>
                        <input type="text" class="form-control" id="correo_electronico" name="correo_electronico"
                               placeholder="Ingresa correo electrónico" style="text-transform:uppercase;" required>
                    </div>
                    <br>
                    <button type="submit" class="btn btn-primary">Guardar</button>
                    <input type="reset" class="btn btn-default" value="Limpiar">
                </form>
            </div>
        </div>
    </body>
</html>

